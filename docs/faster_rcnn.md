Một số lưu ý khi sử dụng Faster R – CNN

SETUP
Clone project Faster R – CNN về tại:
https://github.com/endernewton/tf-faster-rcnn

Phiên bản cài đặt này khá chi tiết, dễ hiểu, code rõ rang và hướng dẫn chi tiết đi kèm. cần lưu ý tuân theo phần Installation của tác giả

Sử file lib/setup.py đúng với GPU mình sử dụng (Tesla K80 sửa sm_52 thành sm_37)
Cài một số package thêm: cython, opencv-python, easydict
Make cython như hướng dẫn của tác giả. Bước 4 có thể bỏ qua do mình ko quan tâm tới COCO vào lúc này. (đứng trong thư mục lib)
 
make clean
make
cd ..

  DEMO
Tải pre-trained model: Mình thích tải trên google drive về hơn. Thông thường khi mới làm quen, hãy thử Resnet 101 train trên VOC 2007 - 2012 https://drive.google.com/drive/folders/0B1_fAEgxdnvJSmF3YUlZcHFqWTQ

Tải về và đặt đúng đường dẫn:

NET=res101
TRAIN_IMDB=voc_2007_trainval+voc_2012_trainval
mkdir -p output/${NET}/${TRAIN_IMDB}
cd output/${NET}/${TRAIN_IMDB}
ln -s ../../../data/voc_2007_trainval+voc_2012_trainval ./default
cd ../../..

Tóm lại, giải nén được pre-trained model trong thư mục:
tf-faster-rcnn/output/res101/voc_2007_trainval+voc_2012_trainval/default

Sau khi tải về, có thể thử chạy demo để đảm bảo mọi thứ đang ổn:
Chạy ./tools/demo.py

Chương trình sẽ báo lỗi do không tìm được màn hình để hiển thị. Để sửa lỗi này, them vào vài dòng trong demo.py
 
import matplotlib
matplotlib.use('Agg')

Hình chưa được lưu xuống (dùng plt.savefig(…)) để lưu file này xuống máy rồi xem sau. (bài tập)
 

Một số lưu ý khác với demo.py:

Cần liệt kê đầy đủ các class, class đầu tiên luôn là __background__


Cần đọc kỹ cái parse_args này để biết cách truyền tham số. cho đúng, chạy đúng mô hình

Sau này, khi train với dataset của mình, số lượng class có thể thay đổi, kích thước anchor cũng có thể thay đổi, nên nhớ sửa dòng này cho đúng:




TRAIN
Tải pretrain của imagenet về lưu trong data theo đường dẫn data/imagenet_weights. Các pretrain model có thể tải về từ https://github.com/tensorflow/models/tree/master/research/slim#pre-trained-models
Khi train, chỉ có một lệnh đơn giản là train được:
	./experiments/scripts/train_faster_rcnn.sh [GPU_ID] [DATASET] [NET]
Tuy nhiên, chúng ta cần tạo dataset riêng của mình bằng việc thêm vào các file sau:
Nguyên tắc: copy dataset pascal_voc ra và sửa lại theo ý mình.
Thêm vào lib/datasets/factory.py
Tạo ra 2 file [datasetname] và [datasetname_eval] tương tự như VOC. Đối với mỗi file cần lưu ý:

+ Cập nhật lại các classes:


+  (đường dẫn đến thư mục chứa hình ảnh để train)


	Ở đây, ta thường có hai file: Main/trainval.txt và Main/test.txt.
	Trong 2 file này sẽ chứa danh sách các hình ảnh được dung để train và test. Mỗi hình ảnh trên một dòng. Như vậy, trong dataset ta có thể để chung hình ảnh, việc ảnh nào dùng để train hay test sẽ tùy thuộc vào danh sách trong 2 file này.

+  Load annotation (gốc là _load_pascal_annotation(self, index)
Đây là phần cần sửa nhiều nhất, nhớ đánh số từ 0

Đọc hiểu code và sửa theo, sao cho khớp nhất với định dạng file annotate của mình.  Mấy trường khác có thể không cần, không quan tâm, chỉ lưu ý tới tọa độ của 4 góc.

+ Ctrl + F, tìm và sửa các chỗ cần thiết nếu có.

Lưu ý: Vì code này có cơ chế lưu lại cache (để resume) nên trong quá trình debug, tránh bị ức chế thì nhớ delete thư mục cache trước. Thư mục cache nằm ở data/cache.
Kiểm tra kỹ đường dẫn, bước này chỉ rối ở phần đường dẫn, chỉ cần trỏ chính xác thì sẽ xong.-

Tạo thư mục trong data để chứa dữ liệu. Thông thường, dataset sẽ được đặt trong thư mục: name_year (ví dụ: medico_2018). Bên trong có chứa 3 thư mục:
Images chứa ảnh 
Main chứa 2 file text:
Trainval.txt
Test.txt
Meta: chứa annotation file. Mỗi ảnh sẽ phải ứng với một file meta. Ví dụ: 00001.txt và 00001.jpg. Bắt buộc phải có file tương ứng nếu không chương trình sẽ báo lỗi.
 
