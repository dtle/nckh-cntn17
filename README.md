# NCKH-CNTN17

## Nội dung 

## Môi trường cài đặt

Trong hướng dẫn này các bạn sẽ dùng google colab làm môi trường để thực nghiệm. 
Các bạn có thể dùng máy tính cá nhân để thực hiện nếu máy tính các bạn có cấu hình mạnh.

Google Colab có một vài thông tin cần lưu ý như sau:
- Cấu hình: 2 nCPU, 13 Gb RAM, ~25Gb storage
- Mỗi notebook được chạy trong một session. Mỗi session tồn tại tối đa 12h (sau đó sẽ bị xóa).
- Khi ở chế độ nghỉ trong vòng 90 phút, session đó sẽ bị xoá.
- Khi session tắt (hoặc bị xóa), tất cả các tiến trình sẽ mất, tất cả dữ liệu lưu trên storage cung cấp sẽ bị mất. Chỉ dữ liệu lưu trên google drive còn lưu lại.
- Có 3 tùy chọn runtime với: None (CPU only), CPU (K80), TPU

## Thông tin cập nhật hướng dẫn
- notebooks/JupyterLab.ipynb: Cài đặt JupyterLab trên Colab để tiện sử dụng hơn (có thể không dùng)
- notebooks/ObjectDetectionAPI.ipynb: Cài đặt [Object Detection API](https://github.com/tensorflow/models/tree/master/research/object_detection). Hướng dẫn được lấy từ chính project này, có tuỳ chỉnh một vài yếu tố cho phù hợp với Colab. Các bạn có thể xem các hướng dẫn gốc tại [trang chủ project](https://github.com/tensorflow/models/tree/master/research/object_detection).
- pytorch-retinanet: Mã nguồn Retinanet và hướng dẫn cài đặt
- docs/faster_rcnn.md: Một số hướng dẫn và lưu ý khi cài Faster RCNN  
